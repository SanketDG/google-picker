from flask import Flask, escape, request, render_template
import requests

from utils import download_file

app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/upload', methods=['POST'])
def upload():

    API_URL = "https://www.googleapis.com/drive/v2/files/{}"

    if(request.method == 'POST'):
        data = request.get_json(force=True)
        oAuthToken = data['oAuthToken']
        fileId = data['fileId']

        headers = {'Authorization': 'Bearer {}'.format(oAuthToken)}
        json = requests.get(API_URL.format(fileId), headers=headers).json()
        print(json)

        filename = json['title']
        download_url = API_URL.format(fileId) + "?alt=media"
        print(download_url, oAuthToken)
        owner = json['owners'][0]['emailAddress']

        download_file(download_url, filename, owner, headers)

    return render_template("index.html", message="File has been uploaded successfully!")