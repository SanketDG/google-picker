import requests

import os
import shutil

def download_file(url, filename, owner, headers):

    dir_name = os.path.join(os.path.dirname("."), owner)
    if(not os.path.exists(dir_name)):
        os.mkdir(dir_name)

    response = requests.get(url, headers=headers)

    with open(os.path.join(dir_name, filename), 'wb+') as file:
        file.write(response.content)

    # with open(filename, "wb") as f:
    #     f.write(r.content)


    # with requests.get(url, stream=True, headers=headers) as stream:
    #     if(stream.status_code == 200):
    #         print("Stream :" + stream.text)
    #         with open(os.path.join(dir_name, filename), 'wb+') as file:
    #             shutil.copyfileobj(stream.raw, file)